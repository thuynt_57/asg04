/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package oop.asg04;

import java.awt.Dimension;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author Nguyen Thuy
 */
public class JBrainTetris extends JTetris  {
    private JCheckBox brainMode;
    private boolean brainSelected=false;
    private final DefaultBrain defaultBrain;
    private   Brain.Move bestMove;
    private int prevCount=0;
    
    protected JSlider adversary ;
    protected JLabel    adversaryStatus;//web
   // Brain hal2000;//web
    

    public JBrainTetris(int pixels) {
        super(pixels);
        defaultBrain = new DefaultBrain();
    }
    
    @Override
    public void startGame(){
        super.startGame();
        // Set mode based on checkbox at start of game
    }
    
    @Override
    public JComponent createControlPanel(){
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        
        panel= (JPanel) super.createControlPanel();
        
        //BRAIN
        panel.add(new JLabel("Brain:"));
        brainMode = new JCheckBox("Brain active");
        panel.add(brainMode);
        
        //ADVERSARY
        JPanel little = new JPanel();
       // make a little panel, put a JSlider in it. JSlider responds to getValue()
        little = new JPanel();
        little.add(new JLabel("Adversary:"));
        adversary = new JSlider(0, 100, 0); // min, max, current
        adversary.setPreferredSize(new Dimension(100,15));
        little.add(adversary);
        // now add little to panel of controls
        panel.add(little);
        //status text
	 adversaryStatus = new JLabel("OK");
	 panel.add(adversaryStatus);
        
        /*adversary.addChangeListener( new ChangeListener() {
        // when the slider changes, sync the timer to its value
        public void stateChanged(ChangeEvent e) {
               // updateTimer();
        }
        });*/
		
        
        return panel;
        
    }
    
    @Override
    public  void tick(int verb){
        if (!gameOn) return;
        
       // int result = setCurrent(newPiece, newX, newY);
        //boolean failed = (result >= Board.PLACE_OUT_BOUNDS);
        brainSelected = brainMode.isSelected();
        
        //if(brainSelected) 
            if(count==prevCount+1){ 
                if (currentPiece != null) 
			board.undo();	// remove the piece from its old position

                 bestMove=defaultBrain.bestMove(board, currentPiece, 20, null);
                 prevCount=count;
            }
                 //tick(DOWN);
         
        if( ( verb==DOWN ) && ( brainSelected ) ){
           // if(bestMove.piece==null) throw new RuntimeException("you wrong! guy");
            if(bestMove.piece!=currentPiece&&board.getMaxHeight()<=HEIGHT-4) super.tick(ROTATE);
            if(bestMove.x>currentX) super.tick(RIGHT);
            if(bestMove.x<currentX)  super.tick(LEFT);
        }
             
        super.tick(verb);
    }
    
/**
	 Selects the next piece to use using the random generator
	 set in startGame().
     * @return 
	*/
    @Override
	public Piece pickNextPiece() {
		double adValue = adversary.getValue();		
		double numRand = random.nextDouble() * 100;
                
		if (numRand >= adValue ){
			adversaryStatus.setText("ok");
			return super.pickNextPiece();
		}
		else{
			adversaryStatus.setText("*ok*");
			return worstPiece();
		}
	}
	
	/**
	 * @return the worst possible piece to play next, given the incredible ai of the brain
	 */
	private Piece worstPiece(){
		Piece worstPiece = null; 
                double maxScore = 0;
		Piece[] arrayPieces = Piece.getPieces();
		Brain.Move move = null;
		for (Piece piece : arrayPieces){
			board.undo();
			move = defaultBrain.bestMove(board, piece, HEIGHT, move);
			if ( move.score > maxScore ){
				maxScore = move.score;
				worstPiece = piece;
			}
		}
		
		if (worstPiece != null)
			return worstPiece;
		else
			return arrayPieces[0];
		
	}

	
    /*
    Creates a frame with a JBrainTetris.
   */
   public static void main(String[] args) {
           // Set GUI Look And Feel Boilerplate.
           // Do this incantation at the start of main() to tell Swing
           // to use the GUI LookAndFeel of the native platform. It's ok
           // to ignore the exception.
           try {
                   UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
           } catch (Exception ignored) { }

           JBrainTetris brainTetris = new JBrainTetris(16);
           JFrame frame = JBrainTetris.createFrame(brainTetris);
           frame.setVisible(true);
   }
    
    
}
