package oop.asg04;

// Board.java
/**
 * CS108 Tetris Board. Represents a Tetris board -- essentially a 2-d grid of
 * booleans. Supports tetris pieces and row clearing. Has an "undo" feature that
 * allows clients to add and remove pieces efficiently. Does not do any drawing
 * or have any idea of pixels. Instead, just represents the abstract 2-d board.
 */
public class Board {
    // Some ivars are stubbed out for you:

    private int width;
    private int height;
    private boolean[][] grid;
    private boolean DEBUG = true;
    boolean committed;
    int[] widths;
    int[] heights;
    int[] xwidths;//use for back up
    int[] xheights;
    boolean[][] xgrid;
    
    private final static int numberBlock=4;
    
    
  

    // Here a few trivial methods are provided:
    /**
     * Creates an empty board of the given width and height measured in blocks.
     */
    public Board(int width, int height) {
        this.width = width;
        this.height = height;
        grid = new boolean[width][height];
        committed = true;

        // MY CODE HERE
        widths=new int[height];
        for (int i = 0; i < height; i++) 
            widths[i] = 0;
        
        heights=new int[width];
        for (int i = 0; i < width; i++) 
            heights[i] = 0;
        
        //use for back up
        xwidths=new int[height];
        for (int i = 0; i < height; i++) 
            xwidths[i] = 0;
        
        xheights=new int[width];
        for (int i = 0; i < width; i++) 
            xheights[i] = 0;
        
        xgrid = new boolean[width][height]; 
    }

    /**
     * Returns the width of the board in blocks.
     */
    public int getWidth() {
        return width;
    }

    /**
     * Returns the height of the board in blocks.
     */
    public int getHeight() {
        return height;
    }

    /**
     * Returns the max column height present in the board. For an empty board
     * this is 0.
     */
    public int getMaxHeight() {
        //MY CODE HERE
        int maxHeight = 0;

        for (int i = 0; i < width; i++) {
            if (heights[i] > maxHeight) {
                maxHeight = heights[i];
            }
        }

        return maxHeight;
    }

    /**
     * MY CODE HERE
     * find the index y of highest block of column at index x
     * not use heights[]
     * use for sanityCheck()
     */
    private int highestBlock(int x) {
        int index=0;

        for(int j=0;j<height;j++)
           if (grid[x][j] == true) 
                index=j+1;
        return index;
    }
 
    
    
    /**
     * MY CODE HERE
     * find number filled block of the row at index y
     * not use widths[]
     * use for sanityCheck()
     */
    private int numFilledBlock(int y){
            int num=0;
      
            for(int i=0;i<width;i++)
                if(grid[i][y]==true) num++;
         
            return num;
    }
            

    /**
     * Checks the board for internal consistency -- used for debugging.
     */
    public void sanityCheck() {
        if (DEBUG) {
            // MY CODE HERE
            
            //check heights[]
            for (int i = 0; i < width; i++) 
                if (heights[i] != highestBlock(i)) 
                    throw new RuntimeException("Error: Column "+i+" has incorrrect height ");

            //check widths[]
            for (int i = 0; i < height; i++) 
                if(widths[i]!=numFilledBlock(i))
                    throw new RuntimeException("Error: Rows "+i+" has incorrect width");
            
            
            //check maxHeight
            int maxH=0;
            for(int i=0;i<width;i++)
                if(maxH<highestBlock(i)) maxH=highestBlock(i);
            if(maxH!=getMaxHeight()) 
                throw new RuntimeException("Error: Incorrect max height");
            
        }
    }

    /**
     * Given a piece and an x, returns the y value where the piece would come to
     * rest if it were dropped straight down at that x.
     *
     * <p>
     * Implementation: use the skirt and the col heights to compute this fast --
     * O(skirt length).
     */
    public int dropHeight(Piece piece, int x) {
        // MY CODE HERE
        int []skirt= piece.getSkirt();
        int y ,temp,t=0,h=10000;
        for(int i = 0; i < piece.getWidth(); i++){
             temp = skirt[i]-heights[i+x];
             if(temp < h) {
                     h = temp;
                     t = i+x;
             }    
        }
        y=heights[t]-skirt[t-x];       
        return y;
    }
    

    /**
     * Returns the height of the given column -- i.e. the y value of the highest
     * block + 1. The height is 0 if the column contains no blocks.
     */
    public int getColumnHeight(int x) {
        //MY CODE HERE
        return heights[x];
    }

    /**
     * Returns the number of filled blocks in the given row.
     */
    public int getRowWidth(int y) {
        //MY CODE HERE

        return widths[y];
    }

    /**
     * Returns true if the given block is filled in the board. Blocks outside of
     * the valid width/height area always return true.
     */
    public boolean getGrid(int x, int y) {
        //MY CODE HERE
        return grid[x][y];
    }
    public static final int PLACE_OK = 0;
    public static final int PLACE_ROW_FILLED = 1;
    public static final int PLACE_OUT_BOUNDS = 2;
    public static final int PLACE_BAD = 3;

    /**
     * Attempts to add the body of a piece to the board. Copies the piece blocks
     * into the board grid. Returns PLACE_OK for a regular placement, or
     * PLACE_ROW_FILLED for a regular placement that causes at least one row to
     * be filled.
     *
     * <p>Error cases: A placement may fail in two ways. First, if part of the
     * piece may falls out of bounds of the board, PLACE_OUT_BOUNDS is returned.
     * Or the placement may collide with existing blocks in the grid in which
     * case PLACE_BAD is returned. In both error cases, the board may be left in
     * an invalid state. The client can use undo(), to recover the valid,
     * pre-place state.
     */
       public int place(Piece piece, int x, int y) {
        // flag !committed problem 
        if (!committed) {
            throw new RuntimeException("place commit problem");
        }

        int result = PLACE_OK;

        // MY CODE HERE
        backup();
        TPoint []body=piece.getBody();
        int [] skirtmax=new int[numberBlock];
        //find the highest blocks of Pieces 
        for(int i=0;i<piece.getWidth();i++)
            skirtmax[i]=0;
        for(int i=0;i<piece.getWidth();i++)
            for(int j=0;j<numberBlock;j++)
                if(body[j].x==i && skirtmax[i]<body[j].y) skirtmax[i]=body[j].y;
        

        for (int i=0;i<numberBlock;i++){   
            if((body[i].x+x>width-1)||(body[i].y+y>height-1)||(body[i].y+y<0) || ( body[i].x+x < 0 ))
                return PLACE_OUT_BOUNDS;
            
            if(grid[body[i].x+x][body[i].y+y]==true)
                     return PLACE_BAD;
        }
         
         for (int i=0;i<numberBlock;i++){
                    grid[body[i].x+x][body[i].y+y]=true;
                    
                    widths[body[i].y+y]++;
                    
                    if(skirtmax[body[i].x]+y+1>=heights[body[i].x+x])
                         heights[body[i].x+x]=skirtmax[body[i].x]+y+1;
                   
                    if(widths[body[i].y+y]==width)
                        result= PLACE_ROW_FILLED;
                   // if(heights[body[i].x+x]>this.getMaxHeight())
                   //     this.getMaxHeight()=heights[body[i].x+x];
                        
                }
       
        committed=false;
        sanityCheck();
        return result;
    }
    
   

    /**
     * Deletes rows that are filled all the way across, moving things above
     * down. Returns the number of rows cleared.
     */
    public int clearRows() {
        // MY CODE HERE
        if ( committed )
            backup();
        int rowsCleared = 0;
        int _heightMax=getMaxHeight();
        
         for(int t=0;t<_heightMax;t++)
            if(widths[t]==width)            
                rowsCleared++;
        
         for(int i=0;i<_heightMax;i++){
            if(widths[i]==width){
                for(int j=i;j<_heightMax;j++){
                   // System.arraycopy(grid[j+1], 0, grid[j], 0, width); not this way
                    widths[j]=widths[j+1];
                    for(int x=0;x<width;x++)
                        grid[x][j] = grid[x][j+1];
                }
                
                for(int k=0;k<width;k++)
                    heights[k]=highestBlock(k);
                
                i--;
            }
        }
                
        committed=false;
        sanityCheck();
        return rowsCleared;
    }
    
    
    private void backup(){
            for(int i=0;i<width;i++){
               // grid[i]=new boolean[width];
                System.arraycopy(grid[i], 0, xgrid[i], 0, height);
            }

            System.arraycopy(widths, 0, xwidths, 0, height);
            System.arraycopy(heights, 0, xheights, 0, width);
    }

    /**
     * Reverts the board to its state before up to one place and one
     * clearRows(); If the conditions for undo() are not met, such as calling
     * undo() twice in a row, then the second undo() does nothing. See the
     * overview docs.
     */
    public void undo() {
        // MY CODE HERE
        // copy from current state which's before up to one place() and one clearRow()
        if(committed==false){
            // recover from backup state
            boolean[][] grid_temp=grid;
            int [] widths_temp=widths;
            int [] heights_temp=heights;
            
            grid=xgrid;
            widths=xwidths;
            heights=xheights;
            
            xgrid=grid_temp;
            xwidths=widths_temp;
            xheights=heights_temp;
            
            committed = true;
        }
        
    }

    /**
     * Puts the board in the committed state.
     */
    public void commit() {
        committed = true;
    }

    /*
     Renders the board state as a big String, suitable for printing.
     This is the sort of print-obj-state utility that can help see complex
     state change over time.
     (provided debugging utility) 
     */
    public String toString() {
        StringBuilder buff = new StringBuilder();
        for (int y = height - 1; y >= 0; y--) {
            buff.append('|');
            for (int x = 0; x < width; x++) {
                if (getGrid(x, y)) {
                    buff.append('+');
                } else {
                    buff.append(' ');
                }
            }
            buff.append("|\n");
        }
        for (int x = 0; x < width + 2; x++) {
            buff.append('-');
        }
        return (buff.toString());
    }


/*public static  void main(String[] args){
        //////
                Board b = new Board(6, 10);
		
		Piece l2 = new Piece(Piece.L2_STR);
		/*Piece pyr2 = pyr1.computeNextRotation();
		Piece pyr3 = pyr2.computeNextRotation();
		Piece pyr4 = pyr3.computeNextRotation();
                Piece stick = new Piece(Piece.STICK_STR);
		
		Piece s = new Piece(Piece.S1_STR);
		Piece sRotated = s.computeNextRotation();
		
		b.place(l2, 0, 0);
                System.out.print(b.toString());
                b.undo();
                System.out.print(b.toString());
}*/
}
