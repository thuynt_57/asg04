package oop.asg04;

import static org.junit.Assert.*;

import org.junit.*;

public class BoardTest {
	Board b;
        Board c;
	Piece pyr1, pyr2, pyr3, pyr4, s, sRotated,stick;
        Piece[] arrayPieces;

	// This shows how to build things in setUp() to re-use
	// across tests.
	
	// In this case, setUp() makes shapes,
	// and also a 5X10 board, with pyr placed at the bottom,
	// ready to be used by tests.
	@Before
	public void setUp() throws Exception {
		b = new Board(5, 10);
		
		pyr1 = new Piece(Piece.PYRAMID_STR);
		pyr2 = pyr1.computeNextRotation();
		pyr3 = pyr2.computeNextRotation();
		pyr4 = pyr3.computeNextRotation();
                stick = new Piece(Piece.STICK_STR);
		
		s = new Piece(Piece.S1_STR);
		sRotated = s.computeNextRotation();
                
                arrayPieces = Piece.getPieces () ;
	}

        @Test
        public void testPlace (){
            assertEquals ( Board . PLACE_OK , b . place ( arrayPieces [ Piece . L2 ] , 3 , 0 ) ) ;
            b.commit();
            // Check the basic width/height/max after the one placement
            assertEquals(0, b.getColumnHeight(0));
            assertEquals(0, b.getColumnHeight(1));
            assertEquals(0, b.getColumnHeight(2));
            assertEquals(1, b.getColumnHeight(3));
            assertEquals(3, b.getColumnHeight(4));
            assertEquals(2, b.getRowWidth(0));
            assertEquals(1, b.getRowWidth(1));
            assertEquals(1, b.getRowWidth(2));
            assertEquals(0, b.getRowWidth(3));
            assertEquals(3, b.getMaxHeight());
            // Check the basic width/height/max after the two placement
            assertEquals ( Board . PLACE_ROW_FILLED , b . place ( pyr1, 0 , 0 ) ) ;
            assertEquals(1, b.getColumnHeight(0));
            assertEquals(2, b.getColumnHeight(1));
            assertEquals(1, b.getColumnHeight(2));
            assertEquals(1, b.getColumnHeight(3));
            assertEquals(3, b.getColumnHeight(4));
            assertEquals(true, b.getGrid(4,2));
            assertEquals(3, b.getMaxHeight());
            b.commit();
            assertEquals ( Board . PLACE_OK , b . place ( arrayPieces[Piece.SQUARE], 2 , 1 ) ) ;
            b.commit();
            assertEquals ( Board . PLACE_ROW_FILLED , b . place ( arrayPieces[Piece.STICK], 0 , 1) ) ;
            assertEquals(5, b.getRowWidth(0));
            assertEquals(5, b.getRowWidth(1));
            b.commit();
            assertEquals ( Board . PLACE_ROW_FILLED , b . place ( arrayPieces[Piece.STICK], 1 , 2) ) ;
            b.commit();
            assertEquals ( Board . PLACE_ROW_FILLED , b . place ( arrayPieces[Piece.PYRAMID], 2 , 3) ) ;
            assertEquals(5, b.getRowWidth(2));
            assertEquals(5, b.getRowWidth(3));
            assertEquals ( 4 , b .clearRows() ) ;
         
    }
        
        @Test
        public void testDropHeight( ) {
            c = new Board ( 6 , 10 ) ;
            assertEquals ( Board . PLACE_OK , c . place ( arrayPieces[Piece.L2], 0 , 0) ) ;
            c . commit ( ) ;
            assertEquals ( Board . PLACE_OK , c . place ( arrayPieces[Piece.S1], 2 , 0) ) ;
            assertEquals ( 3 , c . dropHeight ( arrayPieces [ Piece . STICK ] . fastRotation ( ) , 0 ) ) ;
            assertEquals ( 3 , c . dropHeight ( arrayPieces [ Piece . STICK ] . fastRotation ( ) , 1 ) ) ;
            assertEquals ( 2 , c . dropHeight ( arrayPieces [ Piece . STICK ] . fastRotation ( ) , 2 ) ) ;
            assertEquals ( 3 , c . dropHeight ( arrayPieces [ Piece . L1 ] . fastRotation ( ) , 0 ) ) ;
            assertEquals ( 2 , c . dropHeight ( arrayPieces [ Piece . L1 ] . fastRotation ( ) , 2 ) ) ;
            assertEquals ( 3 , c . dropHeight ( arrayPieces [ Piece . L1 ] . fastRotation ( ) . fastRotation ( ) , 0 ) ) ;
            assertEquals ( 1 , c . dropHeight ( arrayPieces [ Piece . L1 ] . fastRotation ( ) . fastRotation ( ) , 1 ) ) ;
            assertEquals ( 0 , c . dropHeight ( arrayPieces [ Piece . L1 ] . fastRotation ( ) . fastRotation ( ) , 4 ) ) ;
            assertEquals ( 3 , c . dropHeight ( arrayPieces [ Piece . L1 ] , 0 ) ) ;
            assertEquals ( 3 , c . dropHeight ( arrayPieces [ Piece . L1 ] , 1 ) ) ;
            assertEquals ( 2 , c . dropHeight ( arrayPieces [ Piece . L1 ] , 2 ) ) ;
            assertEquals ( 2 , c . dropHeight ( arrayPieces [ Piece . L1 ] , 4 ) ) ;
            assertEquals ( 2 , c . dropHeight ( arrayPieces [ Piece . L1 ] . fastRotation ( ) . fastRotation ( ) . fastRotation ( ) , 0 ) ) ;
            assertEquals ( 1 , c . dropHeight ( arrayPieces [ Piece . L1 ] . fastRotation ( ) . fastRotation ( ) . fastRotation ( ) , 2 ) ) ;
            assertEquals ( 2 , c . dropHeight ( arrayPieces [ Piece . L1 ] . fastRotation ( ) . fastRotation ( ) . fastRotation ( ) , 3 ) ) ;

            assertEquals ( 2 , c . dropHeight ( arrayPieces [ Piece . L2 ] . fastRotation ( ) , 1 ) ) ;        
            assertEquals ( 1 , c . dropHeight ( arrayPieces [ Piece . L2 ] . fastRotation ( ) , 3 ) ) ;
            assertEquals ( 1 , c . dropHeight ( arrayPieces [ Piece . STICK ] , 0 ) ) ;
            assertEquals ( 3 , c . dropHeight ( arrayPieces [ Piece . STICK ] , 1 ) ) ;
            assertEquals ( 2 , c . dropHeight ( arrayPieces [ Piece . STICK ] , 3 ) ) ;
            assertEquals ( 0 , c . dropHeight ( arrayPieces [ Piece . STICK ] , 5 ) ) ;
            assertEquals ( 1 , c . dropHeight ( arrayPieces [ Piece . L2 ] . fastRotation ( ) . fastRotation ( ) , 0 ) ) ;
            assertEquals ( 1 , c . dropHeight ( arrayPieces [ Piece . L2 ] . fastRotation ( ) . fastRotation ( ) , 2 ) ) ;
            assertEquals ( 2 , c . dropHeight ( arrayPieces [ Piece . L2 ] . fastRotation ( ) . fastRotation ( ) , 4 ) ) ;
    }
        @Test
        public void placeInsertion_undo(){
            Board d = new Board ( 5 , 6 ) ;
            assertEquals ( Board . PLACE_OK , d . place ( arrayPieces[Piece.S1], 0 , 0) ) ;
            d . commit ( ) ;
            assertEquals(1, d.getColumnHeight(0));
            assertEquals(2, d.getColumnHeight(1));
            assertEquals(2, d.getColumnHeight(2));
            assertEquals(0, d.getColumnHeight(3));
            assertEquals(0, d.getColumnHeight(4));
            assertEquals(true, d.getGrid(2,1));
            assertEquals(false, d.getGrid(2,0));
            assertEquals ( Board . PLACE_OK , d . place ( arrayPieces[Piece.L2], 2 , 0) ) ;
            assertEquals(1, d.getColumnHeight(0));
            assertEquals(2, d.getColumnHeight(1));
            assertEquals(2, d.getColumnHeight(2));
            assertEquals(3, d.getColumnHeight(3));
            assertEquals(0, d.getColumnHeight(4));
            assertEquals(true, d.getGrid(2,1));
            assertEquals(true, d.getGrid(2,0));
            assertEquals(3, d.getMaxHeight());
            d.undo();
            assertEquals(1, d.getColumnHeight(0));
            assertEquals(2, d.getColumnHeight(1));
            assertEquals(2, d.getColumnHeight(2));
            assertEquals(0, d.getColumnHeight(3));
            assertEquals(0, d.getColumnHeight(4));
            assertEquals(true, d.getGrid(2,1));
            assertEquals(false, d.getGrid(2,0));
            assertEquals ( Board . PLACE_OK , d . place ( arrayPieces[Piece.L2], 2 , 0) ) ;
            d.commit();
            assertEquals ( Board . PLACE_OK , d . place ( arrayPieces[Piece.L1].fastRotation().fastRotation(), 3 , 1) ) ;
            assertEquals(1, d.getColumnHeight(0));
            assertEquals(2, d.getColumnHeight(1));
            assertEquals(2, d.getColumnHeight(2));
            assertEquals(4, d.getColumnHeight(3));
            assertEquals(4, d.getColumnHeight(4));
            assertEquals(true, d.getGrid(4,1));
            assertEquals(false, d.getGrid(4,0));
            
    }
}
