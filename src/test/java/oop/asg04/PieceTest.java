package oop.asg04;

import java.util.Arrays;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

/*
+  Unit test for Piece class -- starter shell.
+  
+ */
public class PieceTest{
    // You can create data to be used in the your
    // test cases like this. For each run of a test method,
    // a new PieceTest object is created and setUp() is called
    // automatically by JUnit.
    // For example, the code below sets up some
    // pyramid and s pieces in instance variables
    // that can be used in tests.

    private Piece s1, s2, stick, l1, l2, square2, pyramid;
    //
    private Piece pyr1, pyr2, pyr3, pyr4;
    private Piece s, sRotated;
    private Piece stick1 , stick2 ;
    private Piece l11 , l12 , l13 , l14 ;
    private Piece l21 , l22 , l23 , l24 ;
    private Piece s11 , s12 ;
    private Piece s21 , s22 ;
    private Piece square ,squareRotated;

    private Piece [ ] arrayOfGetPieces ;
    @Before
    public void setUp() throws Exception {
        //Pyramid piece
        pyr1 = new Piece(Piece.PYRAMID_STR);
        pyr2 = pyr1.computeNextRotation();
        pyr3 = pyr2.computeNextRotation();
        pyr4 = pyr3.computeNextRotation();

        //s1 piece
        s = new Piece(Piece.S1_STR);
        sRotated = s.computeNextRotation();
        
        //stick piece
        stick1 = new Piece ( Piece . STICK_STR ) ;
        stick2 = stick1 . computeNextRotation ( ) ;

        //l1 piece
        l11 = new Piece ( Piece . L1_STR ) ;
        l12 = l11 . computeNextRotation ( ) ;
        l13 = l12 . computeNextRotation ( ) ;
        l14 = l13 . computeNextRotation ( ) ;

        //L2 piece
        l21 = new Piece ( Piece . L2_STR ) ;
        l22 = l21 . computeNextRotation ( ) ;
        l23 = l22 . computeNextRotation ( ) ;
        l24 = l23 . computeNextRotation ( ) ;

        //S1 piece
        s11 = new Piece ( Piece . S1_STR ) ;
        s12 = s11 . computeNextRotation ( ) ;

        //S2 piece
        s21 = new Piece ( Piece . S2_STR ) ;
        s22 = s21 . computeNextRotation ( ) ;

        //Square piece
        square = new Piece ( Piece . SQUARE_STR ) ;
        squareRotated=square.computeNextRotation();
        
        //
        arrayOfGetPieces = Piece . getPieces ( ) ;
        
        //
        s1 = new Piece("0 0 1 0	 1 1  2 1");
        s2 = new Piece("0 1	1 1  1 0  2 0");
        l1 = new Piece("0 0	0 1	 0 2  1 0");
        l2 = new Piece("0 0	1 0 1 1	 1 2");
        stick = new Piece("0 0	0 1	 0 2  0 3");
        square2 = new Piece("0 0  0 1  1 0  1 1");
        pyramid = new Piece("0 0  1 0  1 1  2 0");
    }

    // Here are some sample tests to get you started
   

    @Test
    public void testSizeOfPiecesBeforeAndAfterRotationOfStickPiece() {
        //Check size of stick piece
        assertEquals(1,stick1.getWidth());
        assertEquals(4,stick1.getHeight());
        //after rotation
        assertEquals(4,stick2.getWidth());
        assertEquals(1,stick2.getHeight());
    }
        
    
    @Test
    public void testSizeOfPiecesBeforeAndAfterRotationOfPyramidPiece() {
        // Check size of pyr piece
        assertEquals(3, pyr1.getWidth());
        assertEquals(2, pyr1.getHeight());
        // after rotation
        assertEquals(2, pyr2.getWidth());
        assertEquals(3, pyr2.getHeight());
        assertEquals(3,pyr3.getWidth());
        assertEquals(2, pyr3.getHeight());
    }
    
    
    @Test
    public void testSizeOfPiecesBeforeAndAfterRotationOfL1Piece() {

        //l1 piece
        assertEquals(2, l11.getWidth());
        assertEquals(3, l11.getHeight());
        //after totation
        assertEquals(3, l12.getWidth());
        assertEquals(2, l12.getHeight());
        assertEquals(2, l13.getWidth());
        assertEquals(3, l13.getHeight());
    }
    
    
    @Test
    public void testSizeOfPiecesBeforeAndAfterRotationOfL2Piece() {
        //l2 piece
        assertEquals ( 2 , l21 . getWidth ( ) ) ;
        assertEquals ( 3 , l21 . getHeight ( ) ) ;   
        //after rotation
        assertEquals ( 3 , l22 . getWidth ( ) ) ;
        assertEquals ( 2 , l22 . getHeight ( ) ) ;              
        assertEquals ( 2 , l23 . getWidth ( ) ) ;
        assertEquals ( 3 , l23 . getHeight ( ) ) ;              
        assertEquals ( 3 , l24 . getWidth ( ) ) ;
        assertEquals ( 2 , l24 . getHeight ( ) ) ;
    }
        
    
    @Test
    public void testSizeOfPiecesBeforeAndAfterRotationOfS1Piece() {
        //s1 piece
        assertEquals(3, s11.getWidth());
        assertEquals(2, s11.getHeight());
        //after totation
        assertEquals(2, s12.getWidth());
        assertEquals(3, s12.getHeight());
    }
    
    
    @Test
    public void testSizeOfPiecesBeforeAndAfterRotationOfSquarePiece() {
        //square piece
        assertEquals(2, square.getWidth());
        assertEquals(2, square.getHeight());
        //after totation
        assertEquals(2, squareRotated.getWidth());
        assertEquals(2, squareRotated.getHeight());
        
    }


    // Test the skirt returned by a few pieces
    
    @Test    public void testSkirtOfPiecesBeforeAndAfterRotationOfStickPiece() {
        // Note must use assertTrue(Arrays.equals(... 
        // as plain .equals does not work right for arrays.
        
        //stick piece
        assertTrue(Arrays.equals(new int[] {0}, stick1.getSkirt()));
        //after rotation
        assertTrue(Arrays.equals(new int[] {0, 0, 0, 0}, stick2.getSkirt()));
    }
        
    @Test
    public void testSkirtOfPiecesBeforeAndAfterRotationOfPramidPiece() {
        //pramid piece
        assertTrue(Arrays.equals(new int[] {0, 0, 0}, pyr1.getSkirt()));
        //after rotation
        assertTrue ( Arrays . equals ( new int [ ]  { 1 , 0 } , pyr2 . getSkirt ( ) ) ) ;
        assertTrue ( Arrays . equals ( new int [ ]  { 1 , 0 , 1 } , pyr3 . getSkirt ( ) ) ) ;
        assertTrue ( Arrays . equals ( new int [ ]  { 0 , 1 } , pyr4 . getSkirt ( ) ) ) ;
    }
    
    @Test
    public void testSkirtOfPiecesBeforeAndAfterRotationOfL1Piece() {

        //L1 piece
        assertTrue(Arrays.equals(new int[] {0, 0}, l11.getSkirt()));
        //after rotation
        assertTrue(Arrays.equals(new int[] {0, 0, 0}, l12.getSkirt()));
        assertTrue(Arrays.equals(new int[] {2, 0}, l13.getSkirt()));
        assertTrue(Arrays.equals(new int[] {0, 1, 1}, l14.getSkirt()));
    }
        
    @Test    public void testSkirtOfPiecesBeforeAndAfterRotationOfL2Piece() {
       //l2 piece
        assertTrue ( Arrays . equals ( new int [ ]  { 0 , 0 } , l21 . getSkirt ( ) ) ) ;
        assertTrue ( Arrays . equals ( new int [ ]  { 1 , 1 , 0 } , l22 . getSkirt ( ) ) ) ;
        assertTrue ( Arrays . equals ( new int [ ]  { 0 , 2 } , l23 . getSkirt ( ) ) ) ;
        assertTrue ( Arrays . equals ( new int [ ]  { 0 , 0 , 0 } , l24 . getSkirt ( ) ) ) ;
    }
    
    @Test
    public void testSkirtOfPiecesBeforeAndAfterRotationOfS1Piece() {
        
        //S1 piece
        assertTrue(Arrays.equals(new int[] {0, 0, 1}, s.getSkirt()));
        //after rotation
        assertTrue(Arrays.equals(new int[] {1, 0}, sRotated.getSkirt()));
    }

    @Test
    public void testFastRotationAndEqualsOfStickPiece ( )  { 
        assertTrue ( stick1.equals( arrayOfGetPieces [ 0 ] )) ;
        assertTrue (stick2. equals ( arrayOfGetPieces [ 0 ] . fastRotation ( ) ) ) ;
        assertTrue ( stick1 . equals ( arrayOfGetPieces [ 0 ] . fastRotation ( ) . fastRotation ( ) ) ) ;
    }
      
    @Test
    public void testFastRotationAndEqualsOfL1Piece ( )  { 
        assertTrue ( l11 . equals ( arrayOfGetPieces [ 1 ] ) ) ;
        assertTrue ( l12 . equals ( arrayOfGetPieces [ 1 ] . fastRotation ( ) ) ) ;
        assertTrue ( l13 . equals ( arrayOfGetPieces [ 1 ] . fastRotation ( ) . fastRotation ( ) ) ) ;
        assertTrue ( l14 . equals ( arrayOfGetPieces [ 1 ] . fastRotation ( ) . fastRotation ( ) . fastRotation ( ) ) ) ;
        assertTrue ( l11 . equals ( arrayOfGetPieces [ 1 ] . fastRotation ( ) . fastRotation ( ) . fastRotation ( ) . fastRotation ( ) ) ) ;
    }
    
    @Test
    public void testFastRotationOfL2 ( )  { 
        assertTrue ( l21 . equals ( arrayOfGetPieces [ 2 ] ) ) ;
        assertTrue ( l22 . equals ( arrayOfGetPieces [ 2 ] . fastRotation ( ) ) ) ;
        assertTrue ( l23 . equals ( arrayOfGetPieces [ 2 ] . fastRotation ( ) . fastRotation ( ) ) ) ;
        assertTrue ( l24 . equals ( arrayOfGetPieces [ 2 ] . fastRotation ( ) . fastRotation ( ) . fastRotation ( ) ) ) ;
        assertTrue ( l21 . equals ( arrayOfGetPieces [ 2 ] . fastRotation ( ) . fastRotation ( ) . fastRotation ( ) . fastRotation ( )) ) ;
    }
    
    @Test
    public void testFastRotationOfS1 ( )  { 
        assertTrue ( s11 . equals ( arrayOfGetPieces [ 3 ] ) ) ;
        assertTrue ( s12. equals ( arrayOfGetPieces [ 3 ] . fastRotation ( ) ) ) ;
        assertTrue ( s11 . equals ( arrayOfGetPieces [ 3 ] . fastRotation ( ) . fastRotation ( ) ) ) ;
    }
    
    @Test
    public void testFastRotationOfS2 ( )  { 
        
        assertTrue ( s21 . equals ( arrayOfGetPieces [ 4 ] ) ) ;
        assertTrue ( s22. equals ( arrayOfGetPieces [ 4 ] . fastRotation ( ) ) ) ;
        assertTrue ( s21 . equals ( arrayOfGetPieces [ 4 ] . fastRotation ( ) . fastRotation ( ) ) ) ;
    }
    
    @Test
    public void testFastRotationOfSquare ( )  { 
        
        assertTrue ( square . equals ( arrayOfGetPieces [ 5 ] ) ) ;
        assertTrue ( square. equals ( arrayOfGetPieces [ 5 ] . fastRotation ( ) ) ) ;
    }
    
    @Test
    public void testFastRotationOfPyramid ( )  { 
        
        assertTrue ( pyr1 . equals ( arrayOfGetPieces [ 6 ] ) ) ;
        assertTrue ( pyr2 . equals ( arrayOfGetPieces [ 6 ] . fastRotation ( ) ) ) ;
       assertTrue ( pyr3. equals ( arrayOfGetPieces [ 6 ] . fastRotation ( ) . fastRotation ( ) ) ) ;
        assertTrue ( pyr4. equals ( arrayOfGetPieces [ 6 ] . fastRotation ( ) . fastRotation ( ) . fastRotation ( ) ) ) ;
        assertTrue ( pyr1 . equals ( arrayOfGetPieces [ 6 ] . fastRotation ( ) . fastRotation ( ) . fastRotation ( ) . fastRotation ( ) ) ) ;
    
    }
    
    @Test
    public void makeFastRotationCreatesCircularStructures() {
        Piece[] pieces = Piece.getPieces();
        Piece first = pieces[Piece.STICK];
       assertTrue(first == first.fastRotation().fastRotation());

        first = pieces[Piece.SQUARE];
        assertTrue(first == first.fastRotation());

        first = pieces[Piece.S1];
        assertTrue(first == first.fastRotation().fastRotation().fastRotation().fastRotation());

        first = pieces[Piece.S2];
        assertTrue(first == first.fastRotation().fastRotation().fastRotation().fastRotation());

        first = pieces[Piece.L1];
        assertTrue(first == first.fastRotation().fastRotation().fastRotation().fastRotation());

        first = pieces[Piece.L2];
        assertTrue(first == first.fastRotation().fastRotation().fastRotation().fastRotation());

        first = pieces[Piece.PYRAMID];
        assertTrue(first == first.fastRotation().fastRotation().fastRotation().fastRotation());
    }

 
}