﻿Họ và tên: Nguyễn Thị Thủy
Ngày sinh: 5/10/1994
Mã sinh viên: 12020377
Lớp: K57CA
Mô tả project(lớp Piece):
 - Mục đích:
   + Khởi tạo các mảnh cơ bản 
   + Tính toán các trạng thái xoay của mảnh
 - Các thuộc tính:
   	TPoint[] body;// tọa độ của các điểm thân mảnh
	int[] skirt; //tung độ nhỏ nhất tại mỗi hoành độ của thân mảnh
	int width;// độ rộng của mảnh
	int height;// chiều cao của mảnh
        private Piece[] pieces;// mảng lưu các trạng thái xoay của 7 mảnh cơ bản
	Piece next;// trạng thái tiếp theo của mảnh trong mảng
        final static int numberNode=4;// số khối trong mỗi mảng, ở đây là 4
	
 - Các phương thức chính:
   + Piece(TPoint[] points):tạo một mảnh từ một mảng TPoint
   + Piece(String points): tạo một mảnh từ một xâu 
   + Piece computeNextRotation(): tính toán trạng thái xoay tiếp theo của mảnh
   + boolean equals(Object obj): kiểm tra xem 2 mảnh có bằng nhau không
   + Piece makeFastRotations(Piece root): bắt đầu từ root rồi tạo tất cả các mảnh trong danh sách và nối chúng lại với nhau quanh mảnh ban đầu.
   +  Piece fastRotation(): trả về trạng thái tiếp theo của mảnh
   + Piece[] getPieces(): tính toán trước các trạng thái xoay của mảnh rồi lưu vào mảng, sử dụng makeFastRotation();